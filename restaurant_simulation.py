from restaurant import *
from rostering_agent import RosteringAgent
from forecast import forecast
from employees import employees


def main():

    forecast_board = Board(board_type="forecastBoard", message=forecast)
    employee_board = Board(board_type="employeeBoard", message=employees)
    roster_board = Board(board_type="rosterBoard")

    restaurant = RestaurantEnvironment(forecast_board=forecast_board,
                                       employee_board=employee_board,
                                       roster_board=roster_board)

    rostering_agent = RosteringAgent()
    restaurant.add_thing(rostering_agent)

    restaurant.run(1)


if __name__ == "__main__":
    main()
