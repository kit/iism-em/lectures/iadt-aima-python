from vacuum_task import *


def main():
    vacuum_world = IADTVacuumEnvironment()
    agent = IADTReflexVacuumAgent()

    vacuum_world.add_thing(agent)

    life_time_steps = 1000
    vacuum_world.run(life_time_steps)

    print("Agent performance over its life time of "
          + str(life_time_steps)
          + " steps was "
          + str(agent.performance) + ".")


if __name__ == "__main__":
    main()
