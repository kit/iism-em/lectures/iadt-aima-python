employees = [
  {
    "id": "5ddedba83f6de700081c899a",
    "compensation": 11.5,
    "overtime_bonus": 3,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2"
    ],
    "availability": [
      7,
      19
    ]
  },
  {
    "id": "5ddedba83f6de700081c89d6",
    "compensation": 10.8,
    "overtime_bonus": 7,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2"
    ],
    "availability": [
      8,
      23
    ]
  },
  {
    "id": "5ddedba83f6de700081c894f",
    "compensation": 10.9,
    "overtime_bonus": 8,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88ce"
    ],
    "availability": [
      8,
      20
    ]
  },
  {
    "id": "5ddedba83f6de700081c8959",
    "compensation": 10.9,
    "overtime_bonus": 11,
    "skills": [
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      10,
      18
    ]
  },
  {
    "id": "5ddedba83f6de700081c89bd",
    "compensation": 12.6,
    "overtime_bonus": 6,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2"
    ],
    "availability": [
      11,
      20
    ]
  },
  {
    "id": "5ddedba83f6de700081c89cc",
    "compensation": 11.1,
    "overtime_bonus": 11,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2"
    ],
    "availability": [
      6,
      25
    ]
  },
  {
    "id": "5ddedba83f6de700081c8968",
    "compensation": 12.1,
    "overtime_bonus": 14,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      11,
      25
    ]
  },
  {
    "id": "5ddedba83f6de700081c89c2",
    "compensation": 12.8,
    "overtime_bonus": 2,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      12,
      23
    ]
  },
  {
    "id": "5ddedba83f6de700081c8981",
    "compensation": 12.7,
    "overtime_bonus": 2,
    "skills": [
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2"
    ],
    "availability": [
      9,
      22
    ]
  },
  {
    "id": "5ddedba83f6de700081c895e",
    "compensation": 12.5,
    "overtime_bonus": 8,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      7,
      19
    ]
  },
  {
    "id": "5ddedba83f6de700081c8940",
    "compensation": 11,
    "overtime_bonus": 4,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      9,
      18
    ]
  },
  {
    "id": "5ddedba83f6de700081c8977",
    "compensation": 10.8,
    "overtime_bonus": 11,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      6,
      18
    ]
  },
  {
    "id": "5ddedba83f6de700081c896d",
    "compensation": 10.2,
    "overtime_bonus": 1,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88d2"
    ],
    "availability": [
      10,
      20
    ]
  },
  {
    "id": "5ddedba83f6de700081c898b",
    "compensation": 10.5,
    "overtime_bonus": 6,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      6,
      19
    ]
  },
  {
    "id": "5ddedba73f6de700081c8936",
    "compensation": 13,
    "overtime_bonus": 14,
    "skills": [
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      11,
      20
    ]
  },
  {
    "id": "5ddedba83f6de700081c8995",
    "compensation": 12.5,
    "overtime_bonus": 5,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      6,
      22
    ]
  },
  {
    "id": "5ddedba83f6de700081c89a4",
    "compensation": 10.9,
    "overtime_bonus": 10,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88d2"
    ],
    "availability": [
      10,
      22
    ]
  },
  {
    "id": "5ddedba83f6de700081c8963",
    "compensation": 10.2,
    "overtime_bonus": 9,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      6,
      21
    ]
  },
  {
    "id": "5ddedba83f6de700081c8954",
    "compensation": 10.6,
    "overtime_bonus": 9,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88d2"
    ],
    "availability": [
      7,
      18
    ]
  },
  {
    "id": "5ddedba83f6de700081c89b8",
    "compensation": 11.2,
    "overtime_bonus": 2,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      6,
      22
    ]
  },
  {
    "id": "5ddedba73f6de700081c892c",
    "compensation": 12.2,
    "overtime_bonus": 10,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      7,
      23
    ]
  },
  {
    "id": "5ddedba83f6de700081c899f",
    "compensation": 10.6,
    "overtime_bonus": 2,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88cf"
    ],
    "availability": [
      9,
      23
    ]
  },
  {
    "id": "5ddedba83f6de700081c8990",
    "compensation": 11.4,
    "overtime_bonus": 6,
    "skills": [
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88d2"
    ],
    "availability": [
      12,
      18
    ]
  },
  {
    "id": "5ddedba83f6de700081c8945",
    "compensation": 10,
    "overtime_bonus": 11,
    "skills": [
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      8,
      21
    ]
  },
  {
    "id": "5ddedba83f6de700081c894a",
    "compensation": 12.3,
    "overtime_bonus": 5,
    "skills": [
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      10,
      25
    ]
  },
  {
    "id": "5ddedba83f6de700081c8972",
    "compensation": 10.7,
    "overtime_bonus": 12,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      7,
      23
    ]
  },
  {
    "id": "5ddedba83f6de700081c89b3",
    "compensation": 10.2,
    "overtime_bonus": 9,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2"
    ],
    "availability": [
      6,
      22
    ]
  },
  {
    "id": "5ddedba83f6de700081c89a9",
    "compensation": 11.8,
    "overtime_bonus": 4,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      7,
      23
    ]
  },
  {
    "id": "5ddedba73f6de700081c8931",
    "compensation": 10,
    "overtime_bonus": 8,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2"
    ],
    "availability": [
      10,
      25
    ]
  },
  {
    "id": "5ddedba83f6de700081c897c",
    "compensation": 10.7,
    "overtime_bonus": 1,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      8,
      20
    ]
  },
  {
    "id": "5ddedba83f6de700081c89ae",
    "compensation": 11.1,
    "overtime_bonus": 8,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88cf"
    ],
    "availability": [
      8,
      19
    ]
  },
  {
    "id": "5ddedba83f6de700081c89d1",
    "compensation": 12.2,
    "overtime_bonus": 5,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      8,
      20
    ]
  },
  {
    "id": "5ddedba83f6de700081c8986",
    "compensation": 11.7,
    "overtime_bonus": 13,
    "skills": [
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88cf"
    ],
    "availability": [
      6,
      18
    ]
  },
  {
    "id": "5ddedba83f6de700081c893b",
    "compensation": 10.2,
    "overtime_bonus": 14,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      12,
      21
    ]
  },
  {
    "id": "5ddedba83f6de700081c89c7",
    "compensation": 10.3,
    "overtime_bonus": 9,
    "skills": [
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      9,
      22
    ]
  },
  {
    "id": "5ddedba83f6de700081c8a8f",
    "compensation": 10.5,
    "overtime_bonus": 6,
    "skills": [
      "5ddedba73f6de700081c88cf",
      "5ddedba73f6de700081c88d2",
      "5ddedba73f6de700081c88d3"
    ],
    "availability": [
      8,
      25
    ]
  },
  {
    "id": "5ddedba83f6de700081c8a94",
    "compensation": 10.8,
    "overtime_bonus": 2,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88ce",
      "5ddedba73f6de700081c88cf"
    ],
    "availability": [
      6,
      24
    ]
  },
  {
    "id": "5ddedba83f6de700081c8a8a",
    "compensation": 10.9,
    "overtime_bonus": 3,
    "skills": [
      "5ddedba73f6de700081c88d0",
      "5ddedba73f6de700081c88d1",
      "5ddedba73f6de700081c88ce"
    ],
    "availability": [
      12,
      23
    ]
  },
  {
    "id": "5ddedbb33f6de700081ca7f0",
    "compensation": 12.7,
    "overtime_bonus": 10,
    "skills": [],
    "availability": [
      8,
      18
    ]
  }
]
