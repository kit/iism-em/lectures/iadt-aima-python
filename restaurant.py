from agents import *


class Board(Thing):

    def __init__(self, board_type, message=None):
        self.message = message
        self.board_type = board_type

    def pin(self, message):
        self.message = message

    def take(self):
        message = self.message
        self.message = ""
        return message


class RestaurantEnvironment(Environment):

    def __init__(self, forecast_board, employee_board, roster_board):
        super().__init__()
        self.forecast_board = forecast_board
        self.employee_board = employee_board
        self.roster_board = roster_board

        self.add_thing(self.forecast_board)
        self.add_thing(self.roster_board)

    def percept(self, agent):
        return self.forecast_board.take(), self.employee_board.take()

    def execute_action(self, agent, action):
        self.roster_board.pin(message=action)
