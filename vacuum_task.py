from agents import Agent, SimpleReflexAgentProgram, Environment, loc_A, loc_B
import random


class IADTVacuumEnvironment(Environment):

    def __init__(self):
        super().__init__()
        self.status = {loc_A: random.choice(['Clean', 'Dirty']),
                       loc_B: random.choice(['Clean', 'Dirty'])}

    def thing_classes(self):
        return [IADTReflexVacuumAgent]

    def percept(self, agent):
        """Returns the agent's location, and the location status (Dirty/Clean)."""
        return agent.location, self.status[agent.location]

    def execute_action(self, agent, action):
        """Change agent's location and/or location's status; track performance.
        Score 1 for each clean square at each time step."""
        if action == 'Right':
            agent.location = loc_B
            agent.performance += self.calculate_score_at_time_step()
        elif action == 'Left':
            agent.location = loc_A
            agent.performance += self.calculate_score_at_time_step()
        elif action == 'Suck':
            self.status[agent.location] = 'Clean'
            agent.performance += self.calculate_score_at_time_step()

    def default_location(self, thing):
        """Agents start in either location at random."""
        return random.choice([loc_A, loc_B])

    def calculate_score_at_time_step(self):
        return sum([1 for square_status in self.status.values() if square_status == 'Clean'])


class Rule:

    def __init__(self, state, action):
        self.state = state
        self.action = action

    def matches(self, state):
        return self.state == state


def IADTReflexVacuumAgent():

    rules = [Rule('TileIsDirty', 'Suck'),
             Rule('LocAIsClean', 'Right'),
             Rule('LocBIsClean', 'Left')]

    def interpret_input(percept):
        location, status = percept

        if status == 'Dirty':
            state = 'TileIsDirty'
        elif location == loc_A:
            state = 'LocAIsClean'
        else:
            state = 'LocBIsClean'

        return state

    return Agent(SimpleReflexAgentProgram(rules, interpret_input))
