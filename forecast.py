forecast = [{
        "day": "Monday",
        "targetPlanningPerUnit": [
            {
                "unit": "5ddedba73f6de700081c88ce",
                "targetPlanningPerHour": [
                    {
                        "hour": 8,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 9,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 10,
                        "numberOfRequiredEmployees": 2
                    },
                    {
                        "hour": 11,
                        "numberOfRequiredEmployees": 2
                    },
                    {
                        "hour": 12,
                        "numberOfRequiredEmployees": 3
                    },
                    {
                        "hour": 13,
                        "numberOfRequiredEmployees": 3
                    },
                    {
                        "hour": 14,
                        "numberOfRequiredEmployees": 3
                    },
                    {
                        "hour": 15,
                        "numberOfRequiredEmployees": 2
                    },
                    {
                        "hour": 16,
                        "numberOfRequiredEmployees": 2
                    }
                ]
            },
            {
                "unit": "5ddedba73f6de700081c88cf",
                "targetPlanningPerHour": [
                    {
                        "hour": 8,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 9,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 10,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 11,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 12,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 13,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 14,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 15,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 16,
                        "numberOfRequiredEmployees": 1
                    }
                ]
            },
            {
                "unit": "5ddedba73f6de700081c88d0",
                "targetPlanningPerHour": [
                    {
                        "hour": 8,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 9,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 10,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 11,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 12,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 13,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 14,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 15,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 16,
                        "numberOfRequiredEmployees": 0
                    }
                ]
            },
            {
                "unit": "5ddedba73f6de700081c88d1",
                "targetPlanningPerHour": [
                    {
                        "hour": 8,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 9,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 10,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 11,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 12,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 13,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 14,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 15,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 16,
                        "numberOfRequiredEmployees": 0
                    }
                ]
            },
            {
                "unit": "5ddedba73f6de700081c88d2",
                "targetPlanningPerHour": [
                    {
                        "hour": 8,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 9,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 10,
                        "numberOfRequiredEmployees": 2
                    },
                    {
                        "hour": 11,
                        "numberOfRequiredEmployees": 2
                    },
                    {
                        "hour": 12,
                        "numberOfRequiredEmployees": 2
                    },
                    {
                        "hour": 13,
                        "numberOfRequiredEmployees": 2
                    },
                    {
                        "hour": 14,
                        "numberOfRequiredEmployees": 2
                    },
                    {
                        "hour": 15,
                        "numberOfRequiredEmployees": 2
                    },
                    {
                        "hour": 16,
                        "numberOfRequiredEmployees": 2
                    }
                ]
            },
            {
                "unit": "5ddedba73f6de700081c88d3",
                "targetPlanningPerHour": [
                    {
                        "hour": 8,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 9,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 10,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 11,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 12,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 13,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 14,
                        "numberOfRequiredEmployees": 1
                    },
                    {
                        "hour": 15,
                        "numberOfRequiredEmployees": 0
                    },
                    {
                        "hour": 16,
                        "numberOfRequiredEmployees": 0
                    }
                ]
            }
        ]
    },
    {
        "day": "Tuesday",
        "targetPlanningPerUnit": [
                {
                    "unit": "5ddedba73f6de700081c88ce",
                    "targetPlanningPerHour": [
                        {
                            "hour": 8,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 9,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 10,
                            "numberOfRequiredEmployees": 2
                        },
                        {
                            "hour": 11,
                            "numberOfRequiredEmployees": 2
                        },
                        {
                            "hour": 12,
                            "numberOfRequiredEmployees": 3
                        },
                        {
                            "hour": 13,
                            "numberOfRequiredEmployees": 3
                        },
                        {
                            "hour": 14,
                            "numberOfRequiredEmployees": 3
                        },
                        {
                            "hour": 15,
                            "numberOfRequiredEmployees": 2
                        },
                        {
                            "hour": 16,
                            "numberOfRequiredEmployees": 2
                        }
                    ]
                },
                {
                    "unit": "5ddedba73f6de700081c88cf",
                    "targetPlanningPerHour": [
                        {
                            "hour": 8,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 9,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 10,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 11,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 12,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 13,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 14,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 15,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 16,
                            "numberOfRequiredEmployees": 1
                        }
                    ]
                },
                {
                    "unit": "5ddedba73f6de700081c88d0",
                    "targetPlanningPerHour": [
                        {
                            "hour": 8,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 9,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 10,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 11,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 12,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 13,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 14,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 15,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 16,
                            "numberOfRequiredEmployees": 0
                        }
                    ]
                },
                {
                    "unit": "5ddedba73f6de700081c88d1",
                    "targetPlanningPerHour": [
                        {
                            "hour": 8,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 9,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 10,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 11,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 12,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 13,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 14,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 15,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 16,
                            "numberOfRequiredEmployees": 0
                        }
                    ]
                },
                {
                    "unit": "5ddedba73f6de700081c88d2",
                    "targetPlanningPerHour": [
                        {
                            "hour": 8,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 9,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 10,
                            "numberOfRequiredEmployees": 2
                        },
                        {
                            "hour": 11,
                            "numberOfRequiredEmployees": 2
                        },
                        {
                            "hour": 12,
                            "numberOfRequiredEmployees": 2
                        },
                        {
                            "hour": 13,
                            "numberOfRequiredEmployees": 2
                        },
                        {
                            "hour": 14,
                            "numberOfRequiredEmployees": 2
                        },
                        {
                            "hour": 15,
                            "numberOfRequiredEmployees": 2
                        },
                        {
                            "hour": 16,
                            "numberOfRequiredEmployees": 2
                        }
                    ]
                },
                {
                    "unit": "5ddedba73f6de700081c88d3",
                    "targetPlanningPerHour": [
                        {
                            "hour": 8,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 9,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 10,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 11,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 12,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 13,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 14,
                            "numberOfRequiredEmployees": 1
                        },
                        {
                            "hour": 15,
                            "numberOfRequiredEmployees": 0
                        },
                        {
                            "hour": 16,
                            "numberOfRequiredEmployees": 0
                        }
                    ]
                }
            ]
    },
]
